<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {

        return view('auth.login');
    }

    public function store(Request $request)
    {
       $this->validate($request,[
           'email' => 'required|email',
           'password' => 'required'
       ]);

        return redirect()->route('dashboard');
    }
}
