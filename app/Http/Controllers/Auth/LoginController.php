<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class LoginController extends Controller
{
    public function index()
    {
        return view('auth.login');
    }

    public function validity(Request $request)
    {
        $this->validate($request,[
            'email' => 'required',
            'password' => 'required',
        ]);


//        $credentials = $request->only('email', 'password');
//        if (Auth::attempt($credentials)) {
//            return redirect()->intended('index')
//                ->withSuccess('You have Successfully loggedin');
//        }
//
////        dd($credentials);
//        return redirect("login")->withSuccess('Oppes! You have entered invalid credentials');
//
////        $auth = auth();
////
//        dd($auth);
        if (!auth()->attempt($request->only('email', 'password'))) {

            return back()->with('status', 'invalid login details');
        }

        return redirect()->route('index');

    }
}
