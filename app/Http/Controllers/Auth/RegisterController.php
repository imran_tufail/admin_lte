<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{

    public function index()
    {

    return view('auth.register');

    }

    public function store(Request $request)
    {
//            dd($request);
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required',
            'password' => 'required|confirmed',
        ]);

//        dd($request);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),

        ]);

        return redirect()->route('login');
    }

}
